const app = require("./app");

const dotenv = require("dotenv");

dotenv.config({ path: "backend/config/config.env" });

const connectDatabase = require("./config/database");

// Handling uncaught exception which is console.log(somename which is not defined)
process.on("uncaughtException", (err) => {
  console.log(`Error : ${err.message}`);
  console.log("Shutting down the server due to uncaught exception occured");
  process.exit(1);
});

// calling database connection here..
connectDatabase();

const server = app.listen(process.env.PORT, () => {
  console.log(`server is running under port ${process.env.PORT}`);
});

// unhandled promise rejection
process.on("unhandledRejection", (err) => {
  console.log(`Error : ${err.message}`);
  console.log("shutting down the server due to unhandled rejection");

  server.close(() => {
    process.exit(1);
  });
});
